using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Bot {
	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;
			new Bot(reader, writer, new Join(botName, botKey));
		}
	}
	JArray s;
	private StreamWriter writer;
	int indexOFLastPich=0;
	Bot(StreamReader reader, StreamWriter writer, Join join) {
		this.writer = writer;
		string line;

		send(join);
		
		while((line = reader.ReadLine()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			switch(msg.msgType) {
				case "carPositions":

					JArray a = JArray.Parse(msg.data.ToString());
				int pieceIndex=0;
				int laneIndex=0;
					for (int indexOfCarInArrayCarPos=0; indexOfCarInArrayCarPos < a.Count; indexOfCarInArrayCarPos++)
					{
               			string arr = a[indexOfCarInArrayCarPos].ToString();
						PosCar aaaaaa = JsonConvert.DeserializeObject<PosCar>(arr);
						//Console.WriteLine("in male mane="+aaaaaa.id.name);

						if(aaaaaa.id.name.Equals("Schumachers"))
						{
							pieceIndex=aaaaaa.PiecePosition.pieceIndex;
							laneIndex=aaaaaa.PiecePosition.lane.startLaneIndex;
							//Console.WriteLine("dakhele if : "+aaaaaa.PiecePosition.pieceIndex.ToString ());
							break;
						}
					//Console.WriteLine(sasasas);
						//PosCar aaaaaa = JsonConvert.DeserializeObject<PosCar>(arr);
	
						//if(aaaaaa.id.name.Equals("SCHUMACHERS"))
						//	break;
            		}
				//Console.WriteLine("kharejesh if : "+pieceIndex.ToString ());
				//indexOfCarInArrayCarPos=0;
				//Console.Write(indexOfCarInArrayCarPos.ToString()+"   ");
					JObject o = JObject.Parse(line);
            		//int pieceIndex =(int)o.SelectToken("data["+indexOfCarInArrayCarPos.ToString()+"].piecePosition.pieceIndex");
            		//int laneIndex =(int)o.SelectToken("data["+indexOfCarInArrayCarPos.ToString()+"].piecePosition.lane.startLaneIndex");
				    string LaterPiece=(string)s[(pieceIndex+2)%s.Count]["length"];
				    //string CurrentPiece=(string)s[(pieceIndex)%s.Count]["length"];
					//Console.WriteLine(kkkkk); 
					//Console.WriteLine(Convert.ToString( s.Count));
					
					double speed=1.0;
					if((object)LaterPiece == null)
					{
						double angle=(double)s[(pieceIndex+2)%s.Count]["angle"];
						double radius=(double)s[(pieceIndex+2)%s.Count]["radius"];
					//Console.Write(pieceIndex+2);
						//Console.WriteLine(" : "+angle+" - "+radius);
					double norm=0.45, pich=Math.Abs( angle/radius);
						indexOFLastPich=pieceIndex;

					if(laneIndex==0)
					{
						if(angle>0)
						{
							//send(new ChangeLane("Left"));
							speed=0.5;

						}
						else
						{
							//speed=0.5;
							//send(new ChangeLane("Right"));
							if(pich<norm) speed=0.5;
							else speed=0.4;
						}
					}
					else
					{
						
						if(angle<0)
						{
							//send(new ChangeLane("Left"));
							speed=0.5;

						}
						else
						{
							//speed=0.5;
							//send(new ChangeLane("Right"));
							if(pich<norm) speed=0.5;
							else speed=0.4;
						}
					}
						
						if(Math.Abs( indexOFLastPich-pieceIndex)>6)
						{
							speed/=1.5;
						}
						if(pich>norm) speed*=0.75;
					}
					else
						speed=1.0;

					//Console.WriteLine(" --- "+speed);

					//if((object)CurrentPiece == null)
					//{
					//	if(laneIndex==0)
					//		send(new ChangeLane("Right"));
					//	else
					//		send(new ChangeLane("Left"));
					//}
					//Console.WriteLine((string)s[sww]["length"]);
					send(new Throttle(speed));
					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":
					JObject og= JObject.Parse(line);
		            s= (JArray)og.SelectToken("data.race.track.pieces");
		            
					Console.WriteLine("Race init");
					send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
				case "dnf":
					Console.WriteLine("DNF");
					send(new Ping());
					break;
				default:
					send(new Ping());
					break;
			}
		}
	}

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}
}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}


class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}


public class PosCar
{
    public class Id
    {
        public string name;
        public string color;

    }
    public class piecePosition
    {
        public int pieceIndex;
        public double inPieceDistance;
        public lane lane;
        public int lap;
    }

    public class lane
    {
        public int startLaneIndex;
        public int endLaneIndex;
    }
    public Id id;
    public double angle;
    public piecePosition PiecePosition;
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

class ChangeLane: SendMsg {
	public string value;

	public ChangeLane(string value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "switchLane";
	}
}
